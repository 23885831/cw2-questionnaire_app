@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="font-size: 18px">Create New Questionnaire</div>

                <div class="card-body">
                    <form action="/questionnaires" method="post">
                      @csrf
                      <!--Creates a title for the questionnaire-->
                      <div class="form-group">
                        <label for="title">Title</label>
                        <input name="title" type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter Title">
                        <small id="titleHelp" class="form-text text-muted">Give your questionnaire a title...</small>

                        @error('title')
                          <small class="text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                      <!--Creates a ethicsStatement for the questionnaire-->
                      <div class="form-group">
                        <label for="ethicsStatement">Ethics Statement</label>
                        <input name="ethicsStatement" type="text" class="form-control" id="ethicsStatement" aria-describedby="ethicsStatementHelp" placeholder="Enter Ethics Statement">
                        <small id="ethicsStatementHelp" class="form-text text-muted">Enter a statement that must be approved of... </small>

                        @error('ethicsStatement')
                          <small class="text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                      <!--Submits the  questionnaire-->
                      <button type="submit" class="btn btn-dark btn-block">Create Questionnaire</button>
                      <a href="/home" class="btn btn-block btn-outline-secondary">Back to Dashboard</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
