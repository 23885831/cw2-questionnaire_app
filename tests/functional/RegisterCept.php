<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('register for questionnaire app');

$I->amOnPage('/register');
$I->seeCurrentUrlEquals('/register');

//And
$I->see('Register');
//Then
$I->see('Name');
$I->see('E-Mail Address');
$I->see('Password');
$I->see('Confirm Password');
$I->see('Register');

Auth::loginUsingId(1);
//And
$I->haveRecord('users', [
  'name' => 'Eva',
  'email' => 'EFisher@gmail.com',
  'password' => 'password03'
]);

//Then
$I->click('Register');
//And
$I->amOnPage('/home');
