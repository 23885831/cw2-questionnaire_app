<?php
$I = new FunctionalTester($scenario);

$I->am('responder');
$I->wantTo('complete site test Questionnaire');

//When
$I->amOnPage('/');
$I->see('Questionnaire App');
//And
$I->see('Questionnaires Available...');

//And
$I->click('Site Test');
//Then
$I->amOnPage('/surveys/6-site-test');
//And
$I->see('Site Test');
//Then
$I->see('1');
//Then
$I->see('Do i think this is going to be in the db?');
//And
$I->see('Complete Survey');
//Then
$I->click('Complete Survey');
//And
$I->amOnPage('/success');
