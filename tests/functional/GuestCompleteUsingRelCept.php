<?php
$I = new FunctionalTester($scenario);

$I->am('responder');
$I->wantTo('complete using relationships Questionnaire');

//When
$I->amOnPage('/');
$I->see('Questionnaire App');
//And
$I->see('Questionnaires Available...');

//And
$I->click('Using Relationships');
//Then
$I->amOnPage('/surveys/3-using-relationships');
//And
$I->see('Using Relationships');
//Then
$I->see('1');
$I->see('2');
$I->see('3');
//Then
$I->see('sdcsd');
$I->see('sdcsd');
$I->see('test 2');

//And
$I->see('Complete Survey');
//Then
$I->click('Complete Survey');
//And
$I->amOnPage('/success');
