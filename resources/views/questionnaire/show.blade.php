@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="font-size: 18px">{{ $questionnaire->title }}</div>
                <!--Below is a group of buttons of which allow the researcher to complete 3 diifferent tasks-->
                <div class="card-body">
                  <a class="btn btn-dark" href="/questionnaires/{{ $questionnaire->id }}/questions/create">Create Question</a>
                  <a class="btn btn-dark" href="/surveys/{{ $questionnaire->id }}-{{ Str::slug($questionnaire->title) }}">Complete Questionnaire</a>
                  <a class="btn btn-dark" href="/questionnaires/{{ $questionnaire->id }}/edit">Edit Questionnare</a>
                </div>
            </div>

            @foreach($questionnaire->questions as $question)
              <div class="card mt-3">
                  <div class="card-header">{{ $question->question }}</div> <!--Grabs the question-->

                  <div class="card-body">
                    <ul class="list-group">
                      @foreach($question->answers as $answer)
                        <li class="list-group-item">{{ $answer->answers }}</li> <!--Grabs the answers-->
                      @endforeach
                    </ul>
                  </div>
              </div>
            @endforeach

            <button class="btn btn-info btn-block mt-3" style="width: 500px">Delete Questionnaire</button><!--Deletes the questionnaire-->
        </div>
    </div>
</div>
@endsection
