<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('see welcome page');

//When
$I->amOnPage('/');
$I->see('Questionnaire App');
//And
$I->see('Questionnaires Available...');

//And
$I->see('Sample');
$I->see('Using Relationships');
$I->see('Site Test');

//Then
$I->see('Login');
//and
$I->see('Register');
