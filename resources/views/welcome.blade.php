<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Questionnaire Application</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

          <!--The next div content is to show the quesitonnaires available to the responders without having to log in
              It also allows the the responder to click on one of the 3 live questionnaires to complete -->

            <div class="content">
                <div class="title m-b-md">
                    Questionnaire App
                </div>
                <div>
                </div>
                <div class="card">
                  <h1>Questionnaires Available...</h1>
                  <a href="/surveys/1-sample" class="button1" style="background-color: white; color: black; border: 2px solid #e7e7e7; font-size: 18px; padding: 10px 24px; text-decoration: none;">Sample</a>
                  <a href="/surveys/3-using-relationships" class="button1"style="background-color: white; color: black; border: 2px solid #e7e7e7; font-size: 18px; padding: 10px 24px; text-decoration: none;">Using Relationships</a>
                  <a href="/surveys/6-site-test" class="button1"style="background-color: white; color: black; border: 2px solid #e7e7e7; font-size: 18px; padding: 10px 24px; text-decoration: none;">Site Test</a>
                </div>
            </div>
        </div>
    </body>
</html>
