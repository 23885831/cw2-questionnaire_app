<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('create a new questionnaire');

Auth::loginUsingId(1);

$I->amOnPage('/questionnaires/create');

//And
$I->see('Create New Questionnaire');

//Then
$I->see('Title');

//And
$I->fillField('title', 'title');

//Then
$I->see('Ethics Statement');

//And
$I->fillField('ethicsStatement', 'ethicsStatement');

//Then
$I->click('Create Questionnaire');
$I->amOnPage('/questionnaires {{ $id }}');
