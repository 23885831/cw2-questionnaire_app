<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('edit questionnaire');

Auth::loginUsingId(1);

$I->amOnPage('/questionnaires/3/edit');

 //And
 $I->see('Edit Questionnaire');

 //Then
 $I->see('Title');

 //And
 $I->fillField('title', 'title');

 //Then
 $I->see('Ethics Statement');

 //And
 $I->fillField('ethicsStatement', 'ethicsStatement');

 //Then
 $I->click('Update Questionnaire');
 $I->amOnPage('/questionnaires {{ $id }}');
