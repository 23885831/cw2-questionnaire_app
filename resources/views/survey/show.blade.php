@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

          <h1>{{ $questionnaire->title }}</h1>

          <form action="/surveys/{{ $questionnaire->id}}-{{ Str::slug($questionnaire->title) }}" method="post"> <!--Grabs the questionnaire id and then the title-->
            @csrf

            @foreach($questionnaire->questions as $key => $question)
            <div class="card mt-3">
                <div class="card-header"><strong>{{ $key + 1 }}</strong> {{ $question->question }}</div> <!--Grabs the key of the question and adds 1 onto the key number because it originally begins with id = 0 -->

                <div class="card-body">

                  @error('responses.' . $key. '.answer_id')
                    <small class="text-danger">{{ $message }}</small>
                  @enderror

                  <ul class="list-group">
                    @foreach($question->answers as $answer)
                      <li class="list-group-item">
                        <input type="checkbox" name="responses[{{ $key }}][answer_id]" id="answer{{ $answer->id }}" value="{{ $answer->id }}"> <!--Grabs the answer id and displays the answer-->
                        {{ $answer->answer }}

                        <input type="hidden" name="responses[{{ $key }}][question_id]" value="{{ $question->id}}">
                      </li>
                    @endforeach
                  </ul>
                </div>
            </div>
            @endforeach

            <a class="btn btn-dark mt-3" href=" {{ route('success') }}">Complete Survey</a>
          </form>
        </div>
    </div>
</div>
@endsection
