@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="font-size: 18px">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!--the a href below allows the researcher to go to a page where they can create a new questionnaire to add to the system -->
                    <a href="/questionnaires/create" class="btn btn-dark btn-block">Create New Questionnaire</a>
                </div>

                <!--The next div shows the researcher the questionnaires that they have created -->
                <div class="card-body">
                  <h3>Your Questionnaires</h3>
                  <a href="/questionnaires/1" class="btn btn-info btn-block">Sample</a>
                  <a href="/questionnaires/3" class="btn btn-info btn-block">Using Relationships</a>
                  <a href="/questionnaires/6" class="btn btn-info btn-block">Site Test</a>
                  <a href="/questionnaires/7" class="btn btn-info btn-block">New Questionnaire</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
