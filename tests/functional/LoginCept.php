<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('login to questionnaire app');

$I->amOnPage('/login');
$I->seeCurrentUrlEquals('/login');

//And
$I->see('Login');
//Then
$I->see('E-Mail Address');
$I->see('Password');
$I->see('Login');
$I->see('Forgot Your Password?');
//Then
$I->click('Forgot Your Password?');
//And
$I->amOnPage('/password/reset');

//Back on login page
Auth::loginUsingId(1);
//And
$I->click('Login');
//Then
$I->amOnPage('/home');
