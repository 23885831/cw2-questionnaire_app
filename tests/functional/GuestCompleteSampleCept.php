<?php
$I = new FunctionalTester($scenario);

$I->am('responder');
$I->wantTo('complete sample Questionnaire');

//When
$I->amOnPage('/');
$I->see('Questionnaire App');
//And
$I->see('Questionnaires Available...');

//And
$I->click('Sample');
//Then
$I->amOnPage('/surveys/1-sample');
//And
$I->see('Sample');
//Then
$I->see('1');
$I->see('2');
$I->see('3');
//Then
$I->see('vdvdv');
$I->see('What should we do now?');
$I->see('Test');
//And
$I->see('Complete Survey');
//Then
$I->click('Complete Survey');
//And
$I->amOnPage('/success');
