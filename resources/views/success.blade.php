@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="font-size: 18px">Success</div>
            </div>
            <!--The next div content is to display a success message to the responder after they have completed the questionnaire -->

            <div class="card-body">
              <h3>Thank you for completing our questionnaire!</h3>
            </div>
        </div>
    </div>
</div>
@endsection
