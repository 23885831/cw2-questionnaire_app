<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('create a new question');

Auth::loginUsingId(1);


$I->amOnPage('/questionnaires/7/questions/create');

//And
$I->see('Answer Choices');

//And
$I->see('Choice 1');
$I->see('Choice 2');
$I->see('Choice 3');
$I->see('Choice 4');

//Then
$I->see('Add Question & Answers');
$I->click('Add Question & Answers');

//Then
$I->amOnPage('questionnaires/7');
