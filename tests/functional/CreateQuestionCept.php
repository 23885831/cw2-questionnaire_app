<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('create a new question');

Auth::loginUsingId(1);


$I->amOnPage('/questionnaires/7/questions/create');

//And
$I->see('Create New Question');
$I->see('Question');

//Then
$I->fillField('question[question]', 'question');

//Then
$I->see('Add Question & Answers');

//And
$I->click('Add Question & Answers');

//Then
$I->amOnPage('/questionnaires/7');
