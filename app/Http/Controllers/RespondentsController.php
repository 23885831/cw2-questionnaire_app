<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;

class RespondentsController extends Controller
{
    public function index()
    {
      $questionnaires = Questionnaire::all();

      return view('respondents', compact('questionnaires'));
    }

    public function show($questionnaires)
    {
      $questionnaires->load('questions.answers');

      return view('questionnaire.show', compact('questionnaire'));
    }
}
