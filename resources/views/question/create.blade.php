@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create New Question</div>

                <div class="card-body">
                    <form action="/questionnaires/{{ $questionnaire->id }}/questions" method="post">
                      @csrf
                      <!--Creates a question -->
                      <div class="form-group">
                        <label for="question">Question</label>
                        <small id="questionHelp" class="form-text text-muted">Ask simple questions for results...</small>
                        <input name="question[question]" type="text" class="form-control"
                               value="{{ old('question.question') }}" id="question" aria-describedby="questionHelp" placeholder="Enter Question"> <!--Value keeps question if an error arises in the form-->

                        @error('question.question')
                          <small class="text-danger">{{ $message }}</small> <!--Shows an error message if the question is not entered-->
                        @enderror
                      </div>

                      <!--Creates answers -->
                      <div class="form-group">
                        <fieldset>
                          <legend>Answer Choices</legend>
                          <small id="choicesHelp" class="form-text text-muted">Give answer choices for respondents...</small>

                          <div>
                            <div class="form-group">
                              <label for="answer1">Choice 1</label>
                              <input name="answers[][answer]" type="text"
                                     value="{{ old('answers.0.answer') }}"
                                     class="form-control" id="answer1" aria-describedby="choicesHelp" placeholder="Enter Choice 1"> <!--Value keeps question if an error arises in the form-->

                              @error('answers.0.answer')
                                <small class="text-danger">{{ $message }}</small><!--Shows an error message if the answer is not entered-->
                              @enderror
                            </div>
                          </div>

                          <div>
                            <div class="form-group">
                              <label for="answer2">Choice 2</label>
                              <input name="answers[][answer]" type="text" value="{{ old('answers.1.answer') }}"
                                     class="form-control" id="answer2" aria-describedby="choicesHelp" placeholder="Enter Choice 2">

                              @error('answers.1.answer')
                                <small class="text-danger">{{ $message }}</small>
                              @enderror
                            </div>
                          </div>

                          <div>
                            <div class="form-group">
                              <label for="answer3">Choice 3</label>
                              <input name="answers[][answer]" type="text" value="{{ old('answers.2.answer') }}"
                                     class="form-control" id="answer3" aria-describedby="choicesHelp" placeholder="Enter Choice 3">

                              @error('answers.2.answer')
                                <small class="text-danger">{{ $message }}</small>
                              @enderror
                            </div>
                          </div>

                          <div>
                            <div class="form-group">
                              <label for="answer4">Choice 4</label>
                              <input name="answers[][answer]" type="text" value="{{ old('answers.3.answer') }}"
                                     class="form-control" id="answer4" aria-describedby="choicesHelp" placeholder="Enter Choice 4">

                              @error('answers.3.answer')
                                <small class="text-danger">{{ $message }}</small>
                              @enderror
                            </div>
                          </div>

                        </fieldset>
                      </div>

                      <button type="submit" class="btn btn-primary">Add Question & Answers</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
