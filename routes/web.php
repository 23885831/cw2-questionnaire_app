<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/respondents', 'RespondentsController@index');
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::group(['middleware' => ['web']], function() {

Route::get('/questionnaires/create', 'QuestionnaireController@create');
Route::post('/questionnaires', 'QuestionnaireController@store');
Route::get('/questionnaires/{questionnaire}', 'QuestionnaireController@show');

Route::get('/questionnaires/{questionnaire}/edit', 'QuestionnaireController@edit');
Route::post('/questionnaires/{questionnaire}', 'QuestionnaireController@update');


Route::get('/questionnaires/{questionnaire}/questions/create', 'QuestionController@create');
Route::post('/questionnaires/{questionnaire}/questions', 'QuestionController@store');

Route::get('surveys/{questionnaire}-{slug}', 'SurveyController@show');
Route::post('surveys/{questionnaire}-{slug}', 'SurveyController@store');

Route::get('/success', 'ResponseController@success')->name('success');
});
