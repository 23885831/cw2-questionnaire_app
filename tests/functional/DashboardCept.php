<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('see dashboard');

Auth::loginUsingId(1);

$I->amOnPage('/home');

//And
$I->see('Dashboard');
//Then
$I->see('Create New Questionnaire');
//Then
$I->see('Your Questionnaires');
//And
$I->see('Sample');
$I->see('Using Relationships');
$I->see('Site Test');
$I->see('New Questionnaire');
